import * as React from 'react';
import {
    View,
    Image
} from 'react-native';

function logo() {
    return (
        <View style={{ marginTop: 30, marginBottom: 50, alignItems: "center" }}>
            <Image style={{ width: 100, height: 100, borderColor: "#c5c5c5", borderWidth: 1, borderRadius: 50 }}
                source={require('./images/logo.png')} />
        </View>
    )
}

export default logo;