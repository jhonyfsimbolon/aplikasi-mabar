import * as React from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    Button,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';
import Logo from './Logo';

function home({ navigation }) {
    return (
        <View style={{ marginLeft: 40, marginRight: 40, flex: 1 }}>
            <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
                <TouchableOpacity style={{
                    backgroundColor: "#FF4545",
                    padding: 3,
                    borderRadius: 12,
                    marginTop: 10,
                    borderWidth:1
                }}>
                    <Text style={{ fontSize: 12, color: "white" }}> LOG OUT </Text>
                </TouchableOpacity>
            </View>
            <Logo />
            <Text style={{ marginBottom: 20, fontSize: 28, alignSelf:"center" }}>PILIH TEMPAT</Text>

            <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom:10 }}>
                <TouchableOpacity style={{
                    width: 90,
                    backgroundColor: "#00B448",
                    alignItems: "center",
                    borderRadius: 20
                }}>
                    <Text style={{ color: "white" }}>Futsal</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    width: 90,
                    backgroundColor: "#00FFA3",
                    alignItems: "center",
                    borderRadius: 20
                }}>
                    <Text style={{}}>Badminton</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{
                    width: 90,
                    backgroundColor: "#00FFA3",
                    alignItems: "center",
                    borderRadius: 20
                }}>
                    <Text style={{}}>Voli</Text>
                </TouchableOpacity>
            </View>
            <View style={{
                height: 320,
                backgroundColor: "white",
                borderRadius: 12,
                borderWidth: 1,
                borderColor: "#9745FF",
                paddingRight: 5,
                paddingLeft: 5
            }}>
                <ScrollView>
                    <TouchableOpacity style={{
                        backgroundColor: "#FF4545",
                        padding: 10,
                        borderRadius: 12,
                        height: 90,
                        margin:2
                    }}>
                        <Text style={{ fontSize: 25, color: "white", marginBottom:12 }}>Nama Tempat</Text>
                        <Text style={{ fontSize: 13, color: "white", alignSelf: "flex-end" }}>Jl. Alamat No.7</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        backgroundColor: "#4584FF",
                        padding: 10,
                        borderRadius: 12,
                        height: 90,
                        margin: 2
                    }}>
                        <Text style={{ fontSize: 25, color: "white", marginBottom: 12 }}>Nama Tempat</Text>
                        <Text style={{ fontSize: 13, color: "white", alignSelf: "flex-end" }}>Jl. Alamat No.7</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        backgroundColor: "#45FF6E",
                        padding: 10,
                        borderRadius: 12,
                        height: 90,
                        margin: 2
                    }}>
                        <Text style={{ fontSize: 25, color: "white", marginBottom: 12 }}>Nama Tempat</Text>
                        <Text style={{ fontSize: 13, color: "white", alignSelf: "flex-end" }}>Jl. Alamat No.7</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        backgroundColor: "#9745FF",
                        padding: 10,
                        borderRadius: 12,
                        height: 90,
                        margin: 2
                    }}>
                        <Text style={{ fontSize: 25, color: "white", marginBottom: 12 }}>Nama Tempat</Text>
                        <Text style={{ fontSize: 13, color: "white", alignSelf: "flex-end" }}>Jl. Alamat No.7</Text>
                    </TouchableOpacity>

                </ScrollView>
            </View>


        </View>
    )
}

export default home;