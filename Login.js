import * as React from 'react';
import {
    StyleSheet,
    View, 
    Text, 
    Button,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';
import Logo from './Logo';

function login({navigation}){
    return (
        <View style={{ marginLeft: 40, marginRight: 40 }}>
            <View style={{ marginTop: 40 }} >
                <Logo />
            </View>
            
            <Text style={{ marginBottom: 5 }}>Username :</Text>
            <TextInput style={{
                padding: 10,
                marginBottom: 15,
                borderWidth: 1,
                borderColor: "#E5E5E5",
                backgroundColor: "#ffffff",
                borderRadius: 5,
                height:30
            }} />
            <Text style={{ marginBottom: 5 }}>Password :</Text>
            <TextInput style={{
                padding: 10,
                marginBottom: 15,
                borderWidth: 1,
                borderColor: "#E5E5E5",
                backgroundColor: "#ffffff",
                borderRadius: 5,
                height: 30
            }} />

            <View style={{ flexDirection: "row", alignSelf: "center", marginTop: 40 }}>
                <TouchableOpacity style={{ marginRight: 10 }} onPress={() => navigation.navigate('Home')}>
                    <View style={{
                        width: 100,
                        alignItems: "center",
                        backgroundColor: "#5eff5e",
                        borderWidth: 2,
                        borderRadius: 10,
                        padding: 7,
                        borderColor: "#c5c5c5"
                    }}>
                        <Text>LOGIN</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginLeft: 10 }} onPress={() => navigation.navigate('SignUp')}>
                    <View style={{
                        width: 100,
                        alignItems: "center",
                        backgroundColor: "#5E5EFF",
                    borderWidth: 2,
                    borderRadius: 10,
                    padding: 7,
                    borderColor: "#c5c5c5"
                }}>
                        <Text>SIGN UP</Text>
                    </View>
                </TouchableOpacity>
            </View>
            

        </View>
            )
}

export default login;